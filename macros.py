#!/usr/bin/python

def screenshot(name, group):
    data = dict()
    data['group'] = group
    data['imageurl'] = "img/%s.png" % name
    data['thumburl'] = "img/%s.thumb.png" % name
    return "<a data-lightbox='%(group)s' href='%(imageurl)s'><img src='%(thumburl)s' /></a>" % data
